

package com.cloudera.flume.source;

public class TwitterSourceConstants {

  public static final String CONSUMER_KEY_KEY = "consumerKey";
  public static final String CONSUMER_SECRET_KEY = "consumerSecret";
  public static final String ACCESS_TOKEN_KEY = "accessToken";
  public static final String ACCESS_TOKEN_SECRET_KEY = "accessTokenSecret";

  public static final String CONSUMER_KEY_KEY_4_REDIS = "consumerKey4Redis";
  public static final String CONSUMER_SECRET_KEY_4_REDIS = "consumerSecret4Redis";
  public static final String ACCESS_TOKEN_KEY_4_REDIS = "accessToken4Redis";
  public static final String ACCESS_TOKEN_SECRET_KEY_4_REDIS = "accessTokenSecret4Redis";

  public static final String HOST="host";
  public static final String PORT="port";
  public static final String AUTHENTICATION="authentication";
  
  public static final String BATCH_SIZE_KEY = "batchSize";
  public static final long DEFAULT_BATCH_SIZE = 1000L;

  public static final String KEYWORDS_KEY = "keywords";
}
