

package com.cloudera.flume.source;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
/**
 * Created by DDR on 7/13/2015.
 */
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.EventDrivenSource;
import org.apache.flume.channel.ChannelProcessor;
import org.apache.flume.conf.Configurable;
import org.apache.flume.event.EventBuilder;
import org.apache.flume.source.AbstractSource;

import org.apache.log4j.Logger;
import twitter4j.*;

import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;



public class TwitterInterns extends AbstractSource
        implements EventDrivenSource, Configurable {
    private static String host;
    private static String port;
    private static String authentication;

    public static String stringContainsItemFromList(String inputString)
    {
        String items[]=redisConnection.getScreenNameArray(host, port, authentication);
        for(int i =0; i < items.length; i++)
        {
            if(inputString.contains(items[i]))
            {
                return items[i];
            }
        }
        return "false";
    }



    private static final Logger logger =
            Logger.getLogger(TwitterInterns.class);


    private String consumerKey;
    private String consumerSecret;
    private String accessToken;
    private String accessTokenSecret;
    private String[] keywords;
    private long[] twitterid;
    long l=Long.MAX_VALUE;


    private TwitterStream twitterStream;
    private Twitter twitter;


    @Override
    public void configure(Context context) {
        consumerKey = context.getString(TwitterSourceConstants.CONSUMER_KEY_KEY);
        consumerSecret = context.getString(TwitterSourceConstants.CONSUMER_SECRET_KEY);
        accessToken = context.getString(TwitterSourceConstants.ACCESS_TOKEN_KEY);
        accessTokenSecret = context.getString(TwitterSourceConstants.ACCESS_TOKEN_SECRET_KEY);


        host=context.getString(TwitterSourceConstants.HOST);
        port=context.getString(TwitterSourceConstants.PORT);
        authentication=context.getString(TwitterSourceConstants.AUTHENTICATION);

        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setOAuthConsumerKey(consumerKey);
        cb.setOAuthConsumerSecret(consumerSecret);
        cb.setOAuthAccessToken(accessToken);
        cb.setOAuthAccessTokenSecret(accessTokenSecret);
        cb.setJSONStoreEnabled(true);
        cb.setIncludeEntitiesEnabled(true);

        Configuration builder = cb.build();
        twitterStream = new TwitterStreamFactory(builder).getInstance();

  }

    @Override
    public void start() {
        logger.info("Started TwitterInterns");
        final ChannelProcessor channel = getChannelProcessor();
        final Map<String, String> headers = new HashMap<String, String>();


        try {
            twitterid= redisConnection.redisConnect(host,port,authentication);
        } catch (IOException e) {
            e.printStackTrace();
        }

        StatusListener listener = new StatusListener() {
            public void onStatus(Status status) {
                String string=new String();

                if(status.isRetweet()==false) {
                    string = stringContainsItemFromList(status.getText());
                    if (string.equals("false") == false)
                    {
                        String celebrityName=redisConnection.getCelebrityName(host,port,authentication,string);
                        headers.put("type", "t");
                        headers.put("isstructured", "true");
                        headers.put("celebrity", celebrityName);
                        String url= "https://twitter.com/" + status.getUser().getScreenName()
                                + "/status/" + status.getId();
                        headers.put("link",url);
                        Event event = null;
                        String str = status.getText();
                        logger.info(status.getText() + ".Name:" + celebrityName+"\nurl="+status.getURLEntities().toString());
                        event = EventBuilder.withBody(str.getBytes(), headers);
                        channel.processEvent(event);
                    }
                }
            }

            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
            }

            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
            }

            public void onScrubGeo(long userId, long upToStatusId) {
            }

            public void onException(Exception ex) {
            }

            public void onStallWarning(StallWarning warning) {
            }
        };

        twitterStream.addListener(listener);
        FilterQuery query = new FilterQuery().follow(twitterid);
        twitterStream.filter(query);

        super.start();
    }


    @Override
    public void stop() {

        twitterStream.shutdown();
        super.stop();
    }
}
