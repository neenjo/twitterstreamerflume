package com.cloudera.flume.source;

import redis.clients.jedis.Jedis;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;

/**
 * Created by DDR on 7/13/2015.
 */
public class redisConnection {
    private String consumerKey;
    private String consumerSecret;
    private String accessToken;
    private String accessTokenSecret;

    private static final Logger logger =
            Logger.getLogger(redisConnection.class);
    static long [] redisConnect(String host, String port, String authentication) throws IOException {




        Jedis jedis=new Jedis(host,Integer.parseInt(port));
        jedis.auth(authentication);
        Set<String> memberArray=  jedis.smembers("twitterid");
        int i=0;
        int length;
        length = memberArray.size();
        long [] twitterid;
        twitterid=new long[length];
        for (String s : memberArray) {
          twitterid[i]=Long.parseLong(s);
            i++;
        }
    return twitterid;
    }
    static String [] getScreenNameArray(String host, String port, String authentication)

    {
        int i=0;
        Jedis jedis=new Jedis(host,Integer.parseInt(port));
        jedis.auth(authentication);
        Set<String> memberArray=  jedis.smembers("celebrities");
        String [] str = new String[2*memberArray.size()];         //change here
        for (String s:memberArray)
        {
            Set<String> memberArray2=  jedis.smembers(s+"Tw");
            for(String s2:memberArray2)
            {
                str[i]=s2;
                i++;
            }

        }
        return str;
    }
    static String getCelebrityName(String host, String port, String authentication,String screenName)

    {
        Jedis jedis=new Jedis(host,Integer.parseInt(port));
        jedis.auth(authentication);
        Set<String> memberArray=  jedis.smembers("celebrities");//change here
        for (String s:memberArray)
        {
            Set<String> memberArray2=  jedis.smembers(s+"Tw");
            for(String s2:memberArray2)
            {
                if(s2.equals(screenName))
                {
                    return s;
                }
            }

        }
       return "false";
    }
}
